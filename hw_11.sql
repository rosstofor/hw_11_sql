-- Database: hw_11

-- DROP DATABASE hw_11;

CREATE DATABASE hw_11
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Ukraine.1251'
    LC_CTYPE = 'Russian_Ukraine.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
CREATE TABLE IF NOT EXISTS cars  (name varchar(100), price_uah float, price_usd float, model varchar(50), year int);

-- Homework
--   |
--   |
--   \/
CREATE OR REPLACE FUNCTION set_uah() RETURNS TRIGGER AS $$
	BEGIN
		NEW.price_uah = NEW.price_usd * 27.0;
		RETURN NEW;
	END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS convert_uah ON cars;

CREATE TRIGGER convert_uah
	BEFORE INSERT OR UPDATE ON cars
	FOR EACH ROW
	EXECUTE PROCEDURE set_uah()
	
SELECT name, model, year, price_uah, price_usd FROM cars;
--   /\
--    |
--    |

INSERT INTO cars (name, price_usd, model, year) values ('santa fe', 20000, 'hyundai', 2018);

INSERT INTO cars (name, price_usd, model, year) values ('i30', 15000, 'hyundai', 2019),
('i20', 12000, 'hyundai', 2018),
('Tucson', 20000, 'hyundai', 2013),
('Accent', 18000, 'hyundai', 2020),
('Camry', 24000, 'Toyota', 2017),
('RAV4', 22000, 'Toyota', 2015),
('Land Cruiser', 30000, 'Toyota', 2014); 

CREATE TABLE company (name varchar(50) PRIMARY KEY unique, country varchar(100), year_est int);

INSERT INTO company (name, country, year_est) values ('hyundai', 'South Korea', 1967),  ('Toyota', 'Japan', 1937);

select model, max(price_uah)
from cars right join company c on cars.model =  c.name 
WHERE year > 2015
GROUP by model
HAVING max(price_uah) >= 20000;


select * from company;